<?php
defined('TYPO3_MODE') || die();

(function () {
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
		'MMC.MmcDirectmailSubscription',
		'Subscr',
		['Subscribe' => 'register, registerConfirm, cancel, cancelConfirm'],
		// non-cacheable actions
		['Subscribe' => 'register, registerConfirm, cancel, cancelConfirm']
	);
})();
