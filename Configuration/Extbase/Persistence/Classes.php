<?php
# model mapping from TYPO3 10, replacing dropped typoscript-configuration, see:
# https://docs.typo3.org/c/typo3/cms-core/master/en-us/Changelog/10.0/Breaking-87623-ReplaceConfigpersistenceclassesTyposcriptConfiguration.html

declare(strict_types = 1);

return [
  \MMC\MmcDirectmailSubscription\Domain\Model\Address::class => [
    'tableName' => 'tt_address',
  ]
];
