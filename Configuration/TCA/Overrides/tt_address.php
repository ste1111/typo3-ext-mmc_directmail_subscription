<?php
defined('TYPO3_MODE') || die();

(function () {
  \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    "tt_address",
    [
      "email_verification_code" => [
        "config" => [
          "type" => "passthrough"
        ]
      ]
    ]
  );
})();
