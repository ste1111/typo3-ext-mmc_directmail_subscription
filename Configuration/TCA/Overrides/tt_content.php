<?php
defined('TYPO3_MODE') || die();

(function () {
  \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'MMC.MmcDirectmailSubscription',
		'Subscr',
  	'MMC directmail subscription'
  );
})();
