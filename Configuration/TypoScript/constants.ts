
plugin.tx_mmcdirectmailsubscription {
	view {
		# cat=plugin.tx_mmcdirectmailsubscription/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:mmc_directmail_subscription/Resources/Private/Templates/
		# cat=plugin.tx_mmcdirectmailsubscription/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:mmc_directmail_subscription/Resources/Private/Partials/
		# cat=plugin.tx_mmcdirectmailsubscription/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:mmc_directmail_subscription/Resources/Private/Layouts/
	}
	settings {
		# cat=plugin.tx_mmcdirectmailsubscription/enable; type=boolean; label=Include jQuery library: disable if jQuery is already available
		includeJQuery = 1
		# cat=plugin.tx_mmcdirectmailsubscription/enable; type=boolean; label=Keep email-address unique: remove tt_address records with the same email-address on subscription
		keepEmailAddressUnique = 1
		# cat=plugin.tx_mmcdirectmailsubscription/email; type=string; label=Email sender address: Email sender address for address-verification-emails
		fromEmail = 
		# cat=plugin.tx_mmcdirectmailsubscription/email; type=string; label=Email sender name: Email sender name for address-verification-emails
		fromName = 
		# cat=plugin.tx_mmcdirectmailsubscription/file; type=string; label=Path to Scripts
		scriptPath = typo3conf/ext/mmc_directmail_subscription/Resources/Public/Scripts/
		# cat=plugin.tx_mmcdirectmailsubscription/enable; type=boolean; label=Remove email-address on cancel subscription: remove tt_address records on cancle subscription. disable to just hide them
		removeAddress = 1
	}
	persistence {
		# cat=plugin.tx_mmcdirectmailsubscription//a; type=string; label=Default storage PID
		storagePid =
	}
}
